

gnuforwin.f90 this Fortran90 module contains a collection of subroutines for plotting data, including 2D, 3D plots, surfaces, polar coordinates and histograms. it is a modification of the GNUFOR2 interface written by Alexey Kuznetsov. GNUFOR2 is a modification of the GNUFOR interface written by John Burkardt: http://orion.math.iastate.edu/burkardt/g_src/gnufor/gnufor.html

این پروژه شامل یک فایل با نام 

gnuforwin.f90

و یک برنامه نمونه برای آزمایش توابع تعریف شده در این مدول است. این مدول برای کامپایلرهای زیر بهینه سازی شده است. 

pgifortran gfortran under mingw32 and mingw-w64

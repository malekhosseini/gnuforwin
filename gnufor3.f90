!> \brief
!! - Author: Seyyed mohammad bagher malekhosseini
!! malekhosseini@vru.ac.ir
!! - Modified: 24 july 2015
!! - this Fortran90 module contains a collection of subroutines for plotting data,
!! including 2D, 3D plots, surfaces, polar coordinates, histograms and encoder for encode multi plot files to movie
!! it is a modification of the GNUFOR2 interface written by Alexey Kuznetsov
!! GNUFOR2 is a modification of the GNUFOR interface written by John Burkardt:
!! http://orion.math.iastate.edu/burkardt/g_src/gnufor/gnufor.html
!!
!***********************************************************************************
module gnufor3
    implicit none
    !***********************************************************************************
    ! these are default parameters which control linewidth, colors and terminal
    !***********************************************************************************
    private
    character(len=200)              :: data_file_name, command_file_name
    character(len=3), parameter     :: default_linewidth='1'
    character(len=100), parameter   :: default_color1='blue'
    character(len=100), parameter   :: default_color2='dark-green'
    character(len=100), parameter   :: default_color3='orange-red'
    character(len=100), parameter   :: default_color4='dark-salmon'
    character(len=100), parameter   :: default_terminal='wxt'
    character(len=100), parameter   :: default_palette='CMY'

    character(len=:), allocatable   :: addition1
    character(len=:), allocatable   :: addition2
    character(len=:), allocatable   :: addition3
    character(len=:), allocatable   :: addition4
    integer                         :: lang
    logical                         :: echo=.false. , always_add_gnuplot=.false.
    public :: plot ,plot3d , surf ,image  , hist, get_unit, my_date_and_time,set_echo,add_gnuplot

    !***********************************************************************************
    interface plot
        module procedure plot_1,plot_1_real4
        module procedure plot_2,plot_2_real4
        module procedure plot_3,plot3d_real4
        module procedure plot_4,plot_4_real4
    end interface
    !***********************************************************************************
    interface surf
        module procedure surf_1,surf_1_real4
        module procedure surf_2,surf_2_real4
        module procedure surf_3,surf_3_real4
    end interface
    !***********************************************************************************
    interface image
        module procedure image_1,image_1_real4
        module procedure image_2,image_2_real4
        module procedure image_3
        module procedure image_4,image_4_real4
    end interface
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
contains
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine set_echo(string,logic)
        implicit none
        character(len=*),optional     :: string
        logical,optional                :: logic
        if(present(string)) then
            if (string=='yes'.or.string=='YES'.or.string=='y'.or.string=='Y') echo=.true.
            if (string=='no' .or.string=='NO' .or.string=='n'.or.string=='N') echo=.false.
        elseif (present(logic)) then
            echo=logic
        else
            echo=.not.echo
        endif

    end subroutine set_echo

    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    function my_date_and_time() result(f_result)
        !***********************************************************************************
        ! this function creates a string with current date and time
        ! it is a default method to name output files
        !***********************************************************************************
        implicit none
        character(len=8)  :: date
        character(len=10) :: time
        character(len=33) :: f_result
        !        integer,save    :: i
        !***********************************************************************************
        call date_and_time(date,time)
        f_result= 'date_'//date(7:8)//'-'//date(5:6)//'-'//date(1:4)//'_time_'//time(1:2)//':'//time(3:4)//':'//time(5:10)
        !***********************************************************************************
    end function my_date_and_time
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    function output_terminal(terminal) result(f_result)
        !***********************************************************************************
        implicit none
        character(len=*),intent(in)    :: terminal
        integer, parameter        :: Nc=35
        character(len=Nc)        :: f_result
        !***********************************************************************************
        select case(terminal)
            case('ps')
                f_result='postscript landscape color'
            case default
                f_result=terminal
        end select
        f_result=trim(f_result)
        !***********************************************************************************
    end function output_terminal
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine image_4_real4(x,y,rgb,pause,terminal,filename,persist,input)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)      :: x(:), y(:)
        real(kind=8)                  ::x8(size(x,1)),y8(size(y,1))
        integer,intent(in)            :: rgb(:,:,:)
        real(kind=4), optional        :: pause
        character(len=*),optional     :: terminal, filename, persist, input
        x8=x
        y8=y
        call image_4(x8,y8,rgb,pause,terminal,filename,persist,input)
    end subroutine image_4_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine image_4(x,y,rgb,pause,terminal,filename,persist,input)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x(:), y(:)
        integer,intent(in)        :: rgb(:,:,:)
        real(kind=4), optional        :: pause
        character(len=*),optional    :: terminal, filename, persist, input
        integer                :: nx, ny
        integer             :: i, j, ierror, ios, file_unit
        character(len=100)        ::  my_pause, my_persist,&
            & xrange1,xrange2,yrange1,yrange2
        !***********************************************************************************
        nx=size(rgb(1,:,1))
        ny=size(rgb(1,1,:))
        if ((size(x).ne.nx).or.(size(y).ne.ny)) then
            ierror=1
            print *,'image2 ERROR: sizes of x(:),y(:) and gray(:,:) are not compatible'
            stop
        end if
        write (xrange1,'(e15.7)') minval(x)
        write (xrange2,'(e15.7)') maxval(x)
        write (yrange1,'(e15.7)') minval(y)
        write (yrange2,'(e15.7)') maxval(y)
        do j=1,ny
            do i=1,nx
                if ((maxval(rgb(:,i,j))>255).or.(minval(rgb(:,i,j))<0)) then
                    print *,'image ERROR: a value of rgb(:,:,:) array is outside [0,255]'
                    stop
                end if
            end do
        end do
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        do j=1,ny
            do i=1,nx
                write (file_unit,'(2E12.4,3I5)') x(i),y(j),rgb(1,i,j),rgb(2,i,j),rgb(3,i,j)
            end do
            write (file_unit,'(a)')
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (present(filename)) then
                write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
            else
                write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                & //trim(my_persist) // ' title  "Gnuplot"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'set nokey'
        write ( file_unit, '(a)' ) 'set xrange ['// trim(xrange1) // ':'// trim(xrange2) //']'
        write ( file_unit, '(a)' ) 'set yrange ['// trim(yrange1) // ':'// trim(yrange2) //']'
        write ( file_unit, '(a)' ) 'unset colorbox'
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'plot "' // trim ( data_file_name ) // &
            & '" with rgbimage'
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine image_4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine image_3(rgb,pause,terminal,filename,persist,input)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        integer, intent(in)        :: rgb(:,:,:)
        real(kind=4), optional        :: pause
        character(len=*),optional    :: terminal, filename, persist, input
        integer                :: nx, ny
        integer             :: i, j, ierror, ios, file_unit
        character(len=100)        ::  my_pause, my_persist
        !***********************************************************************************
        nx=size(rgb(1,:,1))
        ny=size(rgb(1,1,:))
        do j=1,ny
            do i=1,nx
                if ((maxval(rgb(:,i,j))>255).or.(minval(rgb(:,i,j))<0)) then
                    print *,'image ERROR: a value of rgb(:,:,:) array is outside [0,255]'
                    stop
                end if
            end do
        end do
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        do j=1,ny
            do i=1,nx
                write (file_unit,'(5I5)') i,j,rgb(1,i,j),rgb(2,i,j),rgb(3,i,j)
            end do
            write (file_unit,'(a)')
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (present(filename)) then
                write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
            else
                write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                & //trim(my_persist) // ' title  "Gnuplot"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'set nokey'
        write ( file_unit, '(a)' ) 'unset border'
        write ( file_unit, '(a)' ) 'unset xtics'
        write ( file_unit, '(a)' ) 'unset ytics'
        write ( file_unit, '(a)' ) 'unset colorbox'
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'plot "' // trim ( data_file_name ) // &
            & '" with rgbimage'
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine image_3
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine image_2_real4(x,y,gray,pause,palette,terminal,filename,persist,input)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        real(kind=4)                :: x(:), y(:), gray(:,:)
        real(kind=8)               ::x8(size(x,1)),y8(size(y,1)),gray8(size(gray,2),size(gray,1))
        real(kind=4), optional        :: pause
        character(len=*),optional    :: palette, terminal, filename, persist, input
        gray8=gray
        x8=x
        y8=y
        call image_2(x8,y8,gray8,pause,palette,terminal,filename,persist,input)
    end    subroutine image_2_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine image_2(x,y,gray,pause,palette,terminal,filename,persist,input)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x(:), y(:), gray(:,:)
        real(kind=4), optional        :: pause
        character(len=*),optional    :: palette, terminal, filename, persist, input
        integer                :: nx, ny
        integer             :: i, j, ierror, ios, file_unit
        character(len=100)        ::  my_pause, my_persist,&
            & xrange1,xrange2,yrange1,yrange2
        !***********************************************************************************
        nx=size(gray(:,1))
        ny=size(gray(1,:))
        if ((size(x).ne.nx).or.(size(y).ne.ny)) then
            ierror=1
            print *,'image2 ERROR: sizes of x(:),y(:) and gray(:,:) are not compatible'
            stop
        end if
        write (xrange1,'(e15.7)') minval(x)
        write (xrange2,'(e15.7)') maxval(x)
        write (yrange1,'(e15.7)') minval(y)
        write (yrange2,'(e15.7)') maxval(y)
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        do j=1,ny
            do i=1,nx
                write (file_unit,'(3E12.4)') x(i), y(j), gray(i,j)
            end do
            write (file_unit,'(a)')
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (present(filename)) then
                write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
            else
                write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                & //trim(my_persist) // ' title  "Gnuplot"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'set nokey'
        write ( file_unit, '(a)' ) 'set xrange ['// trim(xrange1) // ':'// trim(xrange2) //']'
        write ( file_unit, '(a)' ) 'set yrange ['// trim(yrange1) // ':'// trim(yrange2) //']'
        write ( file_unit, '(a)' ) 'unset colorbox'
        if (present(palette)) then
            if ((trim(palette).ne.'RGB').and.(trim(palette).ne.'HSV').and.(trim(palette).ne.'CMY').and.&
                    & (trim(palette).ne.'YIQ').and.(trim(palette).ne.'XYZ')) then
                write ( file_unit, '(a)' ) 'set palette '// trim(palette)
            else
                write ( file_unit, '(a)' ) 'set palette model '// trim(palette)
            end if
        else
            write ( file_unit, '(a)' ) 'set palette model '// trim(default_palette)
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'plot "' // trim ( data_file_name ) // &
            & '" with image'
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine image_2
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine image_1_real4(gray,pause,palette,terminal,filename,persist,input)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: gray(:,:)
        real(kind=8)                ::gray8(size(gray,2),size(gray,1))
        real(kind=4), optional        :: pause
        character(len=*),optional    :: palette, terminal, filename, persist, input
        gray8=gray
        call image_1(gray8,pause,palette,terminal,filename,persist,input)
    end subroutine image_1_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine image_1(gray,pause,palette,terminal,filename,persist,input)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: gray(:,:)
        real(kind=4), optional        :: pause
        character(len=*),optional    :: palette, terminal, filename, persist, input
        integer                :: nx, ny
        integer             :: i, j, ierror, ios, file_unit
        character(len=100)        ::  my_pause, my_persist
        !***********************************************************************************
        nx=size(gray(:,1))
        ny=size(gray(1,:))
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        do j=1,ny
            do i=1,nx
                write (file_unit,'(I5,I5,E15.7)') i,j,gray(i,j)
            end do
            write (file_unit,'(a)')
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (present(filename)) then
                write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
            else
                write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                & //trim(my_persist) // ' title  "Gnuplot"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'set nokey'
        write ( file_unit, '(a)' ) 'unset border'
        write ( file_unit, '(a)' ) 'unset xtics'
        write ( file_unit, '(a)' ) 'unset ytics'
        write ( file_unit, '(a)' ) 'unset colorbox'
        if (present(palette)) then
            if ((trim(palette).ne.'RGB').and.(trim(palette).ne.'HSV').and.(trim(palette).ne.'CMY').and. &
              & (trim(palette).ne.'YIQ').and.(trim(palette).ne.'XYZ')) then
                write ( file_unit, '(a)' ) 'set palette '// trim(palette)
            else
                write ( file_unit, '(a)' ) 'set palette model '// trim(palette)
            end if
        else
            write ( file_unit, '(a)' ) 'set palette model '// trim(default_palette)
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'plot "' // trim ( data_file_name ) // &
            & '" with image'
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine image_1
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot3d(x,y,z,pause,color,terminal,filename,persist,input,linewidth)
        !***********************************************************************************
        ! this subroutine plots 3D curve, given by three arrays x,y,z
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x(:),y(:),z(:)
        real(kind=4), optional        :: pause, linewidth
        character(len=*),optional    :: color, terminal, filename, persist, input
        integer             :: i, ierror, ios, file_unit, nx
        character(len=100)        ::  my_color, my_pause, my_persist, my_linewidth
        !***********************************************************************************
        ! prepare the data
        nx=size(x)
        if ((size(x).ne.size(y)).or.(size(x).ne.size(z))) then
            print *,'subroutine plot3d ERROR: incompatible sizes of x(:),y(:) and z(:)'
            stop
        end if
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        do i=1,nx
            write (file_unit,'(3E15.7)') x(i), y(i), z(i)
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (present(filename)) then
                write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
            else
                write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                &// trim(my_persist) // ' title "Gnuplot"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'set nokey'
        !***********************************************************************************
        if (present(linewidth)) then
            write (    my_linewidth,'(e9.3)') linewidth
        else
            my_linewidth=trim(default_linewidth)
        end if
        if (present(color)) then
            my_color='"'//trim(color)//'"'
        else
            my_color='"'//trim(default_color1)//'"'
        end if
        write ( file_unit, '(a)' ) 'splot "' // trim ( data_file_name ) // &
            '" using 1:2:3 with lines linecolor rgb' // my_color //' linewidth ' // my_linewidth
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine plot3d
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot3d_real4(x,y,z,pause,color,terminal,filename,persist,input,linewidth)
        !***********************************************************************************
        ! this subroutine plots 3D curve, given by three arrays x,y,z
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: x(:),y(:),z(:)
        real(kind=4), optional        :: pause, linewidth
        character(len=*),optional    :: color, terminal, filename, persist, input
        integer             :: i, ierror, ios, file_unit, nx
        character(len=100)        ::  my_color, my_pause, my_persist, my_linewidth
        !***********************************************************************************
        ! prepare the data
        nx=size(x)
        if ((size(x).ne.size(y)).or.(size(x).ne.size(z))) then
            print *,'subroutine plot3d ERROR: incompatible sizes of x(:),y(:) and z(:)'
            stop
        end if
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        do i=1,nx
            write (file_unit,'(3E15.7)') x(i), y(i), z(i)
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (present(filename)) then
                write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
            else
                write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                &// trim(my_persist) // ' title "Gnuplot"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'set nokey'
        !***********************************************************************************
        if (present(linewidth)) then
            write (    my_linewidth,'(e9.3)') linewidth
        else
            my_linewidth=trim(default_linewidth)
        end if
        if (present(color)) then
            my_color='"'//trim(color)//'"'
        else
            my_color='"'//trim(default_color1)//'"'
        end if
        write ( file_unit, '(a)' ) 'splot "' // trim ( data_file_name ) // &
            '" using 1:2:3 with lines linecolor rgb' // my_color //' linewidth ' // my_linewidth
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine plot3d_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine hist(x,n,pause,color,terminal,filename,persist,input)
        !***********************************************************************************
        ! this subroutine plots the histogram of data contained in array x, using n bins
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x(:) !the data to plot
        integer, intent(in)        :: n !the number of intervals
        real(kind=4), optional        :: pause
        character(len=*),optional    :: color, terminal, filename, persist, input
        integer             :: i, j, ierror, ios, file_unit, nx
        character(len=100)        ::  yrange, xrange1, xrange2, my_color, &
            & xtic_start, dxtic, xtic_end, my_pause, my_persist
        real(kind=8)            :: xmin, xmax, xhist(0:n), yhist(n+1), dx
        !***********************************************************************************
        ! prepare the data
        nx=size(x)
        xmin=minval(x)
        xmax=maxval(x)
        dx=(xmax-xmin)/n
        do i=0,n
            xhist(i)=xmin+i*dx
        end do
        yhist=0.0d0
        do i=1,nx
            j=floor((x(i)-xmin)/dx)+1
            yhist(j)=yhist(j)+1
        end do
        !***********************************************************************************
        write (dxtic,'(e15.7)') dx
        write (yrange,'(e15.7)') maxval(yhist)*1.2
        write (xrange1,'(e15.7)') xmin-(n/10.0)*dx
        write (xrange2,'(e15.7)') xmax+(n/10.0)*dx
        xtic_start=xrange1
        xtic_end=xrange2
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        do i=1,n
            write (file_unit,'(2E15.7)') (xhist(i-1)+0.5*dx), yhist(i)
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (present(filename)) then
                write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
            else
                write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                & //trim(my_persist) // ' title  "Gnuplot"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'set nokey'
        write ( file_unit, '(a)' ) 'set yrange [0.0:'// trim(yrange) //']'
        write ( file_unit, '(a)' ) 'set xrange ['// trim(xrange1) // ':'// trim(xrange2) //']'
        write ( file_unit, '(a)' ) 'set xtic nomirror rotate by -45 '
        write ( file_unit, '(a)' ) 'set xtics '// trim(xtic_start) // ','// trim(dxtic) // ','// trim(xtic_end)
        write ( file_unit, '(a)' ) 'set style data histograms'
        write ( file_unit, '(a)' ) 'set style fill solid border -1'
        !***********************************************************************************
        if (present(color)) then
            my_color='"'//color//'"'
        else
            my_color='"'//trim(default_color1)//'"'
        end if
        write ( file_unit, '(a)' ) 'plot "' // trim ( data_file_name ) // &
            '" using 1:2 with boxes linecolor rgb' // trim(my_color)
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine hist
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine hist_real4(x,n,pause,color,terminal,filename,persist,input)
        !***********************************************************************************
        ! this subroutine plots the histogram of data contained in array x, using n bins
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: x(:) !the data to plot
        real(kind=8)            :: x8(size(x,1))
        integer, intent(in)        :: n !the number of intervals
        real(kind=4), optional        :: pause
        character(len=*),optional    :: color, terminal, filename, persist, input
        x8=x
        call hist(x8,n,pause,color,terminal,filename,persist,input)
        !***********************************************************************************
    end subroutine hist_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine hist_int(x,n,pause,color,terminal,filename,persist,input)
        !***********************************************************************************
        ! this subroutine plots the histogram of data contained in array x, using n bins
        !***********************************************************************************
        implicit none
        integer, intent(in)    :: x(:) !the data to plot
        real(kind=8)            :: x8(size(x,1))
        integer, intent(in)        :: n !the number of intervals
        real(kind=4), optional        :: pause
        character(len=*),optional    :: color, terminal, filename, persist, input
        x8=x
        call hist(x8,n,pause,color,terminal,filename,persist,input)
        !***********************************************************************************
    end subroutine hist_int
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine surf_3(x,y,z,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
        ! this subroutine plots a surface. x and y are arrays needed to generate the x-y grid
        ! z(:,:) is a 2D array
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x(:),y(:),z(:,:)
        real(kind=4), optional        :: pause
        real(kind=8)            :: xyz(3,size(z(:,1)),size(z(1,:)))
        character(len=*),optional    :: palette, terminal, filename,dir, pm3d, contour, persist, input,xlabel,ylabel,title,zlabel
        integer                :: nx, ny
        integer             :: i, j
        !***********************************************************************************
        nx=size(z(:,1))
        ny=size(z(1,:))
        if ((size(x).ne.nx).or.(size(y).ne.ny)) then
            print *,'subroutine surf_3 ERROR: sizes of x(:),y(:), and z(:,:) are incompatible'
            stop
        end if
        !***********************************************************************************
        do i=1,nx
            do j=1,ny
                xyz(1,i,j)=x(i)
                xyz(2,i,j)=y(j)
                xyz(3,i,j)=z(i,j)
            end do
        end do
        call surf_1(xyz,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
    end subroutine surf_3
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine surf_3_real4(x,y,z,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
        ! this subroutine plots a surface. x and y are arrays needed to generate the x-y grid
        ! z(:,:) is a 2D array
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: x(:),y(:),z(:,:)
        real(kind=4), optional        :: pause
        real(kind=8)            :: xyz(3,size(z(:,1)),size(z(1,:)))
        character(len=*),optional    :: palette, terminal, filename,dir, pm3d, contour, persist, input,xlabel,ylabel,title,zlabel
        integer                :: nx, ny
        integer             :: i, j
        !***********************************************************************************
        nx=size(z(:,1))
        ny=size(z(1,:))
        if ((size(x).ne.nx).or.(size(y).ne.ny)) then
            print *,'subroutine surf_3 ERROR: sizes of x(:),y(:), and z(:,:) are incompatible'
            stop
        end if
        !***********************************************************************************
        do i=1,nx
            do j=1,ny
                xyz(1,i,j)=x(i)
                xyz(2,i,j)=y(j)
                xyz(3,i,j)=z(i,j)
            end do
        end do
        call surf_1(xyz,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
    end subroutine surf_3_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine surf_2(z,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
        ! this subroutine plots a surface. The only input is a 2D array z(:,:), the x-y grid
        ! is generated automatically
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: z(:,:)
        real(kind=4), optional        :: pause
        real(kind=8)            :: xyz(3,size(z(:,1)),size(z(1,:)))
        character(len=*),optional    :: palette, terminal, filename,dir, pm3d, contour, persist, input,xlabel,ylabel,title,zlabel
        integer                :: nx, ny
        integer             :: i, j
        !***********************************************************************************
        nx=size(z(:,1))
        ny=size(z(1,:))
        do i=1,nx
            do j=1,ny
                xyz(1,i,j)=dble(i)
                xyz(2,i,j)=dble(j)
                xyz(3,i,j)=z(i,j)
            end do
        end do
        call surf_1(xyz,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
    end subroutine surf_2
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine surf_2_real4(z,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
        ! this subroutine plots a surface. The only input is a 2D array z(:,:), the x-y grid
        ! is generated automatically
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: z(:,:)
        real(kind=4), optional        :: pause
        real(kind=8)            :: xyz(3,size(z(:,1)),size(z(1,:)))
        character(len=*),optional    :: palette, terminal, filename,dir, pm3d, contour, persist, input,xlabel,ylabel,title,zlabel
        integer                :: nx, ny
        integer             :: i, j
        !***********************************************************************************
        nx=size(z(:,1))
        ny=size(z(1,:))
        do i=1,nx
            do j=1,ny
                xyz(1,i,j)=dble(i)
                xyz(2,i,j)=dble(j)
                xyz(3,i,j)=z(i,j)
            end do
        end do
        call surf_1(xyz,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
    end subroutine surf_2_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine surf_1(xyz,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: xyz(:,:,:)
        real(kind=4), optional        :: pause
        character(len=*),optional    :: palette, terminal, filename,dir, pm3d, contour, persist, &
            input,xlabel,ylabel,title,zlabel
        integer                :: nx, ny, nrow
        integer             :: i, j, file_unit
        character(len=100)        ::  my_pause
        !***********************************************************************************
        nx=size(xyz(1,:,1))
        ny=size(xyz(1,1,:))
        nrow=nx*ny
        call initialize_data_file(file_unit,input)
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        do j=1,ny
            do i=1,nx
                write (file_unit,'(3E15.7)') xyz(1:3,i,j)
            end do
            write (file_unit,'(a)')
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        call initialize_command_file(terminal=terminal,filename=filename,dir=dir,persist=persist, &
            file_unit=file_unit,xlabel=xlabel,ylabel=ylabel,title=title)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'set nokey'
        if (present(palette)) then
            if ((trim(palette).ne.'RGB').and.(trim(palette).ne.'HSV').and.(trim(palette).ne.'CMY').and. &
                    (trim(palette).ne.'YIQ').and.(trim(palette).ne.'XYZ')) then
                write ( file_unit, '(a)' ) 'set palette '// trim(palette)
            else
                write ( file_unit, '(a)' ) 'set palette model '// trim(palette)
            end if
        else
            write ( file_unit, '(a)' ) 'set palette model '// trim(default_palette)
        end if
        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        if (present(title))then
            write ( file_unit, '(a)' ) 'set title '//"'"//title//"'"
        endif
        if (present(xlabel))then
            write ( file_unit, '(a)' ) 'set xlabel '//"'"//xlabel//"'"

        endif
        if (present(ylabel))then
            write ( file_unit, '(a)' ) 'set ylabel '//"'"//ylabel//"'"
        endif
        if (present(zlabel))then
            write ( file_unit, '(a)' ) 'set zlabel '//"'"//zlabel//"'"
        endif
        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        !***********************************************************************************
        if (present(pm3d)) then
            write ( file_unit, '(a)' ) 'set '// pm3d
        else
            write ( file_unit, '(a)' ) 'set surface'
        endif
        if (present(contour)) then
            if (contour=='surface') then
                write ( file_unit, '(a)' ) 'set contour surface'
            elseif (contour=='both') then
                write ( file_unit, '(a)' ) 'set contour both'
            else
                write ( file_unit, '(a)' ) 'set contour'
            end if
        end if
        !end if
        write ( file_unit, '(a)' ) 'set hidden3d'
        write ( file_unit, '(a)' ) 'set parametric'
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'splot "' // trim ( data_file_name ) // &
            & '" using 1:2:3 with lines palette'
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine surf_1
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine surf_1_real4(xyz,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
        ! this is the most general subroutine for generating 3D plots.
        ! The data is contained in a 3D array z(:,:,:)
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: xyz(:,:,:)
        real(kind=8) ::xyz8(size(xyz,3),size(xyz,2),size(xyz,1))
        real(kind=4), optional        :: pause
        character(len=*),optional    :: palette, terminal, filename,dir, pm3d, contour, persist, &
            input,xlabel,ylabel,title,zlabel
        xyz8=xyz
        call surf_1(xyz8,pause,palette,terminal,filename,dir,pm3d,contour,persist,input,xlabel,ylabel,title,zlabel)
        !***********************************************************************************
    end subroutine surf_1_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot_4(x1,y1,x2,y2,x3,y3,x4,y4,style,pause,color1,color2,color3,color4,&
            & terminal,filename,dir,polar,persist,input,linewidth, xlabel,ylabel,title)
        !***********************************************************************************
        ! this subroutine plots 4 two-dimensional graphs in the same coordinate system
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x1(:), y1(:), x2(:), y2(:), x3(:), y3(:), x4(:), y4(:)
        real(kind=4), optional        :: pause,linewidth
        character(len=*),optional    :: style, color1, color2, color3, color4, terminal, filename,dir, polar,&
            & persist, input , xlabel,ylabel,title
        integer             :: i, ierror, ios, file_unit, Nx1, Nx2, Nx3, Nx4, Nmax
        character(len=100)        ::  my_linewidth
        integer, parameter        :: Nc=20
        character(len=Nc)        :: my_line_type1, my_line_type2, my_line_type3, my_line_type4, &
            & my_color1, my_color2, my_color3,  my_color4, my_range, my_pause, my_persist
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
        !***********************************************************************************
        Nx1=size(x1)
        Nx2=size(x2)
        Nx3=size(x3)
        Nx4=size(x4)
        if ((size(x1).ne.size(y1)).or.(size(x2).ne.size(y2)).or.(size(x3).ne.size(y3)).or.(size(x4).ne.size(y4))) then
            print *,'subroutine plot ERROR: size(x) is not equal to size(y)'
            stop
        end if
        if (present(style).and.(len(style).ne.12)) then
            print *,'subroutine plot ERROR: argument "style" has wrong number of characters'
            stop
        end if
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_data - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        Nmax=max(Nx1,Nx2,Nx3,Nx4)
        do i=1,Nmax
            write (file_unit,'(8E15.7)') x1(min(i,Nx1)), y1(min(i,Nx1)), x2(min(i,Nx2)), y2(min(i,Nx2)), &
                & x3(min(i,Nx3)), y3(min(i,Nx3)), x4(min(i,Nx4)), y4(min(i,Nx4))
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_data - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_line_type1='lines'
        if (present(style)) then
            if ((style(3:3)=='-')) then
                my_line_type1='linespoints'
            else
                my_line_type1='points'
            end if
        end if
        my_line_type2='lines'
        if (present(style)) then
            if ((style(6:6)=='-')) then
                my_line_type2='linespoints'
            else
                my_line_type2='points'
            end if
        end if
        my_line_type3='lines'
        if (present(style)) then
            if ((style(9:9)=='-')) then
                my_line_type3='linespoints'
            else
                my_line_type3='points'
            end if
        end if
        my_line_type4='lines'
        if (present(style)) then
            if ((style(12:12)=='-')) then
                my_line_type4='linespoints'
            else
                my_line_type4='points'
            end if
        end if
        if (present(linewidth)) then
            write (    my_linewidth,'(e9.3)') linewidth
        else
            my_linewidth=trim(default_linewidth)
        end if
        if (present(color1)) then
            my_color1='"'//trim(color1)//'"'
        else
            my_color1='"'//trim(default_color1)//'"'
        end if
        if (present(color2)) then
            my_color2='"'//trim(color2)//'"'
        else
            my_color2='"'//trim(default_color2)//'"'
        end if
        if (present(color3)) then
            my_color3='"'//trim(color3)//'"'
        else
            my_color3='"'//trim(default_color3)//'"'
        end if
        if (present(color4)) then
            my_color4='"'//trim(color4)//'"'
        else
            my_color4='"'//trim(default_color4)//'"'
        end if
        !***********************************************************************************
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (present(filename)) then
                if (present(dir )) then
                    write ( file_unit, '(a)' ) 'set output "'// trim(dir)// trim(filename) //'"'
                else
                    write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
                endif
            else
                if (present(dir )) then
                    write ( file_unit, '(a)' ) 'set output "'// trim(dir)// trim(my_date_and_time())//'.'//trim(terminal)//'"'
                else
                    write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
                endif
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                & //trim(my_persist) // ' title  "Gnuplot"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'unset key'
        if (present(polar).and.(polar=='yes')) then
            write (my_range,'(e15.7)') max(maxval(abs(y1)),maxval(abs(y2)),maxval(abs(y3)),maxval(abs(y4)))
            write ( file_unit, '(a)' ) 'set xrange [-'//trim(my_range)//':'//trim(my_range)//']'
            write ( file_unit, '(a)' ) 'set yrange [-'//trim(my_range)//':'//trim(my_range)//']'
            write ( file_unit, '(a)' ) 'set size square'
            write ( file_unit, '(a)' ) 'set polar'
            write ( file_unit, '(a)' ) 'set grid polar'
        else
            write ( file_unit, '(a)' ) 'set grid'
        end if
        !***********************************************************************************
        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        if (present(title))then
            write ( file_unit, '(a)' ) 'set title '//"'"//title//"'"
        endif
        if (present(xlabel))then
            write ( file_unit, '(a)' ) 'set xlabel '//"'"//xlabel//"'"
            !        write ( file_unit, '(a)' ) 'set xtics 3'
        endif
        if (present(ylabel))then
            write ( file_unit, '(a)' ) 'set ylabel '//"'"//ylabel//"'"
            !        write ( file_unit, '(a)' ) 'set ytics 1'
        endif
        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        if (present(style)) then
            write ( file_unit, '(a,i2,a)' ) 'plot \"' // trim (data_file_name) &
                &//'\" using 1:2 with ' // trim(my_line_type1) // ' pointtype ' // &
                & style(1:2) // ' linecolor rgb ' // trim(my_color1) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                &//'\" using 3:4 with ' // trim(my_line_type2) // ' pointtype ' &
                &// style(4:5) // ' linecolor rgb ' // trim(my_color2) // ' linewidth '// trim(my_linewidth) //',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                &//'\" using 5:6 with ' // trim(my_line_type3) // ' pointtype ' &
                &// style(7:8) // ' linecolor rgb ' // trim(my_color3) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                &//'\" using 7:8 with ' // trim(my_line_type4) // ' pointtype ' &
                &// style(10:11) // ' linecolor rgb '// trim(my_color4)// ' linewidth '// trim(my_linewidth)
        else
            write ( file_unit, '(a,i2,a)' ) 'plot "' // trim (data_file_name) &
                & //'\" using 1:2 with ' // trim(my_line_type1)  // ' linecolor rgb '&
                & // trim(my_color1) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                & //'\" using 3:4 with ' // trim(my_line_type2)  // ' linecolor rgb '&
                & // trim(my_color2) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                & //'\" using 5:6 with ' // trim(my_line_type3)  // ' linecolor rgb '&
                & // trim(my_color3) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                & //'\" using 7:8 with ' // trim(my_line_type4)  // ' linecolor rgb '&
                & // trim(my_color4) // ' linewidth '// trim(my_linewidth)
        end if
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine plot_4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot_4_real4(x1,y1,x2,y2,x3,y3,x4,y4,style,pause,color1,color2,color3,color4,&
            & terminal,filename,dir,polar,persist,input,linewidth, xlabel,ylabel,title)
        !***********************************************************************************
        ! this subroutine plots 4 two-dimensional graphs in the same coordinate system
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: x1(:), y1(:), x2(:), y2(:), x3(:), y3(:), x4(:), y4(:)
        real(kind=8) ::x18(size(x1,1)), y18(size(y1,1)), &
            x28(size(x2,1)), y28(size(y2,1)), &
            x38(size(x3,1)), y38(size(y3,1)), &
            x48(size(x4,1)), y48(size(y4,1))
        real(kind=4), optional        :: pause,linewidth
        character(len=*),optional    :: style, color1, color2, color3, color4, terminal, filename,dir, polar,&
            & persist, input , xlabel,ylabel,title
        x18=x1
        x28=x2
        x38=x3
        x48=x4
        y18=y1
        y28=y2
        y38=y3
        y48=y4
        call plot_4(x18,y18,x28,y28,x38,y38,x48,y48,style,pause,color1,color2,color3,color4,&
            & terminal,filename,dir,polar,persist,input,linewidth, xlabel,ylabel,title)
        !***********************************************************************************
    end subroutine plot_4_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot_3(x1,y1,x2,y2,x3,y3,style,pause,color1,color2,color3,terminal,filename,dir,polar,persist,input,linewidth, &
            xlabel,ylabel,title)
        !***********************************************************************************
        ! this subroutine plots 3 two-dimensional graphs in the same coordinate system
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x1(:), y1(:), x2(:), y2(:), x3(:), y3(:)
        real(kind=4), optional        :: pause,linewidth
        character(len=*),optional    :: style, color1, color2, color3, terminal, filename,dir,polar, persist, input , &
            xlabel,ylabel,title
        integer             :: i, file_unit, Nx1, Nx2, Nx3, Nmax
        character(len=100)        ::  my_linewidth
        integer, parameter        :: Nc=20
        character(len=Nc)        :: my_line_type1, my_line_type2, my_line_type3, my_color1, my_color2,&
            & my_color3, my_range, my_pause
        !***********************************************************************************
        call initialize_data_file(file_unit,input)
        !***********************************************************************************
        Nx1=size(x1)
        Nx2=size(x2)
        Nx3=size(x3)
        if ((size(x1).ne.size(y1)).or.(size(x2).ne.size(y2)).or.(size(x3).ne.size(y3))) then
            print *,'subroutine plot ERROR: size(x) is not equal to size(y)'
            stop
        end if
        if (present(style).and.(len(style).ne.9)) then
            print *,'subroutine plot ERROR: argument "style" has wrong number of characters'
            stop
        end if

        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        Nmax=max(Nx1,Nx2,Nx3)
        do i=1,Nmax
            write (file_unit,'(6E15.7)') x1(min(i,Nx1)), y1(min(i,Nx1)), x2(min(i,Nx2)), y2(min(i,Nx2)), &
                & x3(min(i,Nx3)), y3(min(i,Nx3))
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        call initialize_command_file(terminal=terminal,filename=filename,dir=dir,persist=persist, &
            file_unit=file_unit,xlabel=xlabel,ylabel=ylabel,title=title)
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_line_type1='lines'
        if (present(style)) then
            if ((style(3:3)=='-')) then
                my_line_type1='linespoints'
            else
                my_line_type1='points'
            end if
        end if
        my_line_type2='lines'
        if (present(style)) then
            if ((style(6:6)=='-')) then
                my_line_type2='linespoints'
            else
                my_line_type2='points'
            end if
        end if
        my_line_type3='lines'
        if (present(style)) then
            if ((style(9:9)=='-')) then
                my_line_type3='linespoints'
            else
                my_line_type3='points'
            end if
        end if
        if (present(linewidth)) then
            write (    my_linewidth,'(e9.3)') linewidth
        else
            my_linewidth=trim(default_linewidth)
        end if
        if (present(color1)) then
            my_color1='"'//trim(color1)//'"'
        else
            my_color1='"'//trim(default_color1)//'"'
        end if
        if (present(color2)) then
            my_color2='"'//trim(color2)//'"'
        else
            my_color2='"'//trim(default_color2)//'"'
        end if
        if (present(color3)) then
            my_color3='"'//trim(color3)//'"'
        else
            my_color3='"'//trim(default_color3)//'"'
        end if
        !***********************************************************************************


        !***********************************************************************************
        write ( file_unit, '(a)' ) 'unset key'
        if (present(polar).and.(polar=='yes')) then
            write (my_range,'(e15.7)') max(maxval(abs(y1)),maxval(abs(y2)),maxval(abs(y3)))
            write ( file_unit, '(a)' ) 'set xrange [-'//trim(my_range)//':'//trim(my_range)//']'
            write ( file_unit, '(a)' ) 'set yrange [-'//trim(my_range)//':'//trim(my_range)//']'
            write ( file_unit, '(a)' ) 'set size square'
            write ( file_unit, '(a)' ) 'set polar'
            write ( file_unit, '(a)' ) 'set grid polar'
        else
            write ( file_unit, '(a)' ) 'set grid'
        end if
        !***********************************************************************************
        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        if (present(title))then
            write ( file_unit, '(a)' ) 'set title '//"'"//title//"'"
        endif
        if (present(xlabel))then
            write ( file_unit, '(a)' ) 'set xlabel '//"'"//xlabel//"'"
            !        write ( file_unit, '(a)' ) 'set xtics 3'
        endif
        if (present(ylabel))then
            write ( file_unit, '(a)' ) 'set ylabel '//"'"//ylabel//"'"
            !        write ( file_unit, '(a)' ) 'set ytics 1'
        endif
        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        if (present(style)) then
            write ( file_unit, '(a,i2,a)' ) 'plot \"' // trim (data_file_name) &
                &//'\" using 1:2 with ' // trim(my_line_type1) // ' pointtype ' // &
                & style(1:2) // ' linecolor rgb ' // trim(my_color1) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                &//'\" using 3:4 with ' // trim(my_line_type2) // ' pointtype ' &
                &// style(4:5) // ' linecolor rgb ' // trim(my_color2) // ' linewidth '// trim(my_linewidth) //',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                &//'\" using 5:6 with ' // trim(my_line_type3) // ' pointtype ' &
                &// style(7:8) // ' linecolor rgb ' // trim(my_color3) // ' linewidth '// trim(my_linewidth)
        else
            write ( file_unit, '(a,i2,a)' ) 'plot \"' // trim (data_file_name) &
                & //'\" using 1:2 with ' // trim(my_line_type1)  // ' linecolor rgb '&
                & // trim(my_color1) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                & //'\" using 3:4 with ' // trim(my_line_type2)  // ' linecolor rgb '&
                & // trim(my_color2) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                & //'\" using 5:6 with ' // trim(my_line_type3)  // ' linecolor rgb '&
                & // trim(my_color3) // ' linewidth '// trim(my_linewidth)
        end if
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot () ;close(file_unit)
        !***********************************************************************************
    end subroutine plot_3
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot_3_real4(x1,y1,x2,y2,x3,y3,style,pause,color1,color2,color3,terminal,filename,dir,polar,persist,input, &
            linewidth,xlabel,ylabel,title)
        !***********************************************************************************
        ! this subroutine plots 3 two-dimensional graphs in the same coordinate system
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: x1(:), y1(:), x2(:), y2(:), x3(:), y3(:)
        real(kind=4), optional        :: pause,linewidth
        character(len=*),optional    :: style, color1, color2, color3, terminal, filename,dir,polar, persist, input, &
            xlabel,ylabel,title

        real(kind=8) ::x18(size(x1,1)), y18(size(y1,1)), &
            x28(size(x2,1)), y28(size(y2,1)), &
            x38(size(x3,1)), y38(size(y3,1))

        x18=x1
        x28=x2
        x38=x3
        y18=y1
        y28=y2
        y38=y3
        call plot_3(x18,y18,x28,y28,x38,y38,style,pause,color1,color2,color3,terminal,filename,dir,polar,persist,input,linewidth, &
            xlabel,ylabel,title)
        !***********************************************************************************
    end subroutine plot_3_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot_2_real4(x1,y1,x2,y2,style,pause,color1,color2,terminal,filename,dir,polar,persist,input,linewidth, &
            xlabel,ylabel,title)
        !***********************************************************************************
        ! this subroutine plots 2 two-dimensional graphs in the same coordinate system
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: x1(:), y1(:), x2(:), y2(:)
        real(kind=4), optional        :: pause,linewidth
        character(len=*),optional    :: style, color1, color2, terminal, filename,dir, polar, persist, input , &
            xlabel,ylabel,title

        real(kind=8) ::x18(size(x1,1)), y18(size(y1,1)), &
            x28(size(x2,1)), y28(size(y2,1))

        x18=x1
        x28=x2

        y18=y1
        y28=y2
        call plot_2(x18,y18,x28,y28,style,pause,color1,color2,terminal,filename,dir,polar,persist,input,linewidth, &
            xlabel,ylabel,title)
        !***********************************************************************************
    end subroutine plot_2_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot_2(x1,y1,x2,y2,style,pause,color1,color2,terminal,filename,dir,polar,persist,input,linewidth, &
            xlabel,ylabel,title)
        !***********************************************************************************
        ! this subroutine plots 2 two-dimensional graphs in the same coordinate system
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x1(:), y1(:), x2(:), y2(:)
        real(kind=4), optional        :: pause,linewidth
        character(len=*),optional    :: style, color1, color2, terminal, filename,dir, polar, persist, input , &
            xlabel,ylabel,title
        integer             :: i,  file_unit, Nx1, Nx2, Nmax
        character(len=100)        ::  my_linewidth
        integer, parameter        :: Nc=20
        character(len=Nc)        :: my_line_type1, my_line_type2, my_color1, my_color2, my_range, my_pause
        !***********************************************************************************
        call initialize_data_file(file_unit,input)
        !***********************************************************************************
        Nx1=size(x1)
        Nx2=size(x2)
        if ((size(x1).ne.size(y1)).or.(size(x2).ne.size(y2))) then
            print *,'subroutine plot ERROR: size(x) is not equal to size(y)'
            stop
        end if
        if (present(style).and.(len(style).ne.6)) then
            print *,'subroutine plot ERROR: argument "style" has wrong number of characters'
            stop
        end if
        !***********************************************************************************

        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************
        Nmax=max(Nx1,Nx2)
        do i=1,Nmax
            write (file_unit,'(4E15.7)') x1(min(i,Nx1)), y1(min(i,Nx1)), x2(min(i,Nx2)), y2(min(i,Nx2))
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        call initialize_command_file(terminal=terminal,filename=filename,dir=dir,persist=persist, &
            file_unit=file_unit,xlabel=xlabel,ylabel=ylabel,title=title)
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_line_type1='lines'
        if (present(style)) then
            if ((style(3:3)=='-')) then
                my_line_type1='linespoints'
            else
                my_line_type1='points'
            end if
        end if
        my_line_type2='lines'
        if (present(style)) then
            if ((style(6:6)=='-')) then
                my_line_type2='linespoints'
            else
                my_line_type2='points'
            end if
        end if
        if (present(linewidth)) then
            write (    my_linewidth,'(e9.3)') linewidth
        else
            my_linewidth=trim(default_linewidth)
        end if
        if (present(color1)) then
            my_color1='"'//trim(color1)//'"'
        else
            my_color1='"'//trim(default_color1)//'"'
        end if
        if (present(color2)) then
            my_color2='"'//trim(color2)//'"'
        else
            my_color2='"'//trim(default_color2)//'"'
        end if
        !***********************************************************************************

        !***********************************************************************************
        write ( file_unit, '(a)' ) 'unset key'
        write ( file_unit, '(a)' ) 'unset key'
        if (present(polar).and.(polar=='yes')) then
            write (my_range,'(e15.7)') max(maxval(abs(y1)),maxval(abs(y2)))
            write ( file_unit, '(a)' ) 'set xrange [-'//trim(my_range)//':'//trim(my_range)//']'
            write ( file_unit, '(a)' ) 'set yrange [-'//trim(my_range)//':'//trim(my_range)//']'
            write ( file_unit, '(a)' ) 'set size square'
            write ( file_unit, '(a)' ) 'set polar'
            write ( file_unit, '(a)' ) 'set grid polar'
        else
            write ( file_unit, '(a)' ) 'set grid'
        end if
        !***********************************************************************************
        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        if (present(style)) then
            write ( file_unit, '(a,i2,a)' ) 'plot \"' // trim (data_file_name) &
                &//'\" using 1:2 with ' // trim(my_line_type1) // ' pointtype ' // &
                & style(1:2) // ' linecolor rgb ' // trim(my_color1) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                &//'\" using 3:4 with ' // trim(my_line_type2) // ' pointtype ' &
                &// style(4:5) // ' linecolor rgb ' // trim(my_color2) // ' linewidth '// trim(my_linewidth)
        else
            write ( file_unit, '(a,i2,a)' ) 'plot \"' // trim (data_file_name) &
                & //'\" using 1:2 with ' // trim(my_line_type1)  // ' linecolor rgb '&
                & // trim(my_color1) // ' linewidth '// trim(my_linewidth) // ',\\'
            write ( file_unit, '(a,i2,a)' ) '     \"'// trim (data_file_name) &
                & //'\" using 3:4 with ' // trim(my_line_type2)  // ' linecolor rgb '&
                & // trim(my_color2) // ' linewidth '// trim(my_linewidth)
        end if
        !***********************************************************************************
        call pause_check(file_unit,pause)
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
        !***********************************************************************************
        call run_gnuplot ()
        !***********************************************************************************
    end subroutine plot_2
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot_1(x1,y1,style,pause,color1,terminal,filename,dir,polar,persist,input,linewidth, &
            xlabel,ylabel,title)
        !***********************************************************************************
        ! this subroutine plots a two-dimensional graph
        !***********************************************************************************
        implicit none
        real(kind=8), intent(in)    :: x1(:), y1(:)
        real(kind=4), optional        :: pause,linewidth
        character(len=*),optional    :: style, color1, terminal, filename,dir, polar, persist, input, &
            xlabel,ylabel,title
        integer             :: i, file_unit, Nx1
        character(len=100)        ::  my_linewidth
        integer, parameter        :: Nc=20
        character(len=Nc)        :: my_line_type1, my_color1, my_range
        !***********************************************************************************
        call initialize_data_file(file_unit,input)
        !***********************************************************************************
        Nx1=size(x1)
        if ((size(x1).ne.size(y1))) then
            print *,'subroutine plot ERROR: size(x) is not equal to size(y)'
            stop
        end if
        if (present(style).and.(len(style).ne.3)) then
            print *,'subroutine plot ERROR: argument "style" has wrong number of characters'
            stop
        end if
        !***********************************************************************************
        ! here we write the date to the data_file - the gnuplot will read this data later
        !***********************************************************************************

        do i=1,Nx1
            write (file_unit,'(2E15.7)') x1(i), y1(i)
        end do
        !***********************************************************************************
        close (unit=file_unit)
        !***********************************************************************************
        call initialize_command_file(terminal=terminal,filename=filename,dir=dir,persist=persist, &
            file_unit=file_unit,xlabel=xlabel,ylabel=ylabel,title=title)
        !***********************************************************************************
        ! here we write the commands to the commands file which gnuplot will execute
        !***********************************************************************************
        my_line_type1='lines'
        if (present(style)) then
            if ((style(3:3)=='-')) then
                my_line_type1='linespoints'
            else
                my_line_type1='points'
            end if
        end if
        if (present(linewidth)) then
            write (    my_linewidth,'(e9.3)') linewidth
        else
            my_linewidth=trim(default_linewidth)
        end if
        if (present(color1)) then
            my_color1='"'//trim(color1)//'"'
        else
            my_color1='"'//trim(default_color1)//'"'
        end if
        !***********************************************************************************

        !***********************************************************************************
        write ( file_unit, '(a)' ) 'unset key'
        if (present(polar).and.(polar=='yes')) then
            write (my_range,'(e15.7)') maxval(abs(y1))
            write ( file_unit, '(a)' ) 'set xrange [-'//trim(my_range)//':'//trim(my_range)//']'
            write ( file_unit, '(a)' ) 'set yrange [-'//trim(my_range)//':'//trim(my_range)//']'
            write ( file_unit, '(a)' ) 'set size square'
            write ( file_unit, '(a)' ) 'set polar'
            write ( file_unit, '(a)' ) 'set grid polar'
        else
            write ( file_unit, '(a)' ) 'set grid'
        end if
        !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        if (present(style)) then
            write ( file_unit, '(a,i2,a)' ) 'plot "' // trim (data_file_name) &
                &//'" using 1:2 with ' // trim(my_line_type1) // ' pointtype ' // &
                & style(1:2) // ' linecolor rgb ' // trim(my_color1) // ' linewidth '// trim(my_linewidth)
        else
            write ( file_unit, '(a,i2,a)' ) 'plot "' // trim (data_file_name) &
                & //'" using 1:2 with ' // trim(my_line_type1)  // ' linecolor rgb '&
                & // trim(my_color1) // ' linewidth '// trim(my_linewidth)
        end if
        !***********************************************************************************
        call pause_check(file_unit,pause)
        close(file_unit)
        !***********************************************************************************
        call run_gnuplot ()
        !***********************************************************************************
    end subroutine plot_1
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine plot_1_real4(x1,y1,style,pause,color1,terminal,filename,dir,polar,persist,input,linewidth, &
            xlabel,ylabel,title)
        !***********************************************************************************
        ! this subroutine plots a two-dimensional graph
        !***********************************************************************************
        implicit none
        real(kind=4), intent(in)    :: x1(:), y1(:)
        real(kind=4), optional        :: pause,linewidth
        character(len=*),optional    :: style, color1, terminal, filename,dir, polar, persist, input, &
            xlabel,ylabel,title

        real(kind=8) ::x18(size(x1,1)), y18(size(y1,1))
        x18=x1
        y18=y1
        call plot_1(x18,y18,style,pause,color1,terminal,filename,dir,polar,persist,input,linewidth, &
            xlabel,ylabel,title)
        !***********************************************************************************
    end subroutine plot_1_real4
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine run_gnuplot()
        !***********************************************************************************
        implicit none
        character (len = 100) command
        integer status
        integer system
        !***********************************************************************************
        !  Issue a command to the system that will startup GNUPLOT, using
        !  the file we just wrote as input.
        !***********************************************************************************
        write (command, *) 'gnuplot ' // trim (command_file_name)
        status=system(trim(command))
        if (status.ne.0) then
            print *,'RUN_GNUPLOT - Fatal error!'
            stop
        end if
        return
        !***********************************************************************************
    end subroutine run_gnuplot
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
    subroutine get_unit(iunit)
        !***********************************************************************************
        implicit none
        integer i
        integer ios
        integer iunit
        logical lopen
        !***********************************************************************************
        iunit=0
        do i=1,99
            if (i/= 5 .and. i/=6) then
                inquire (unit=i, opened=lopen, iostat=ios)
                if (ios==0) then
                    if (.not.lopen) then
                        iunit=i
                        return
                    end if
                end if

            end if
        end do
        return
    end subroutine get_unit
    !***********************************************************************************

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine initialize_command_file(terminal,filename,dir,persist,file_unit,xlabel,ylabel,zlabel,title)
        character(len=*),optional    :: xlabel,ylabel,zlabel,title , terminal, filename,dir, persist!style, color1, ,
        character(len=100)        :: command
        integer :: file_unit,ierror,ios,status
        integer, parameter        :: Nc=20
        character(len=Nc)        ::  my_persist
        integer  system
        ierror = 0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_data - fatal err۴or! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=command_file_name, status='replace',    iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal command file.'
            stop
        end if
        !***********************************************************
        call available_palettes(file_unit)
        my_persist=' '
        if (present(persist).and.(persist=='yes')) my_persist='persist '
        if (present(terminal)) then
            write ( file_unit, '(a)' ) 'set terminal '// trim(output_terminal(terminal))
            if (terminal/='x11') then
                if(terminal/='ps') write ( file_unit, '(a)' ) 'set term '//trim(terminal)
            endif
            if (present(filename)) then
                filename=adjustl(filename)
                if (present(dir )) then
                    write(unit=command,fmt='(a)') "mkdir " //trim(dir)//" 2>/dev/null"
                    status = system(command)
                    write ( file_unit, '(a)' ) 'set output "'// trim(dir)// trim(filename) //'"'
                    if(echo) print*,'start creat '//trim(dir)// trim(filename)
                else
                    write ( file_unit, '(a)' ) 'set output "'// trim(filename) //'"'
                    if(echo) print*,'start creat '//trim(dir)// trim(filename)
                endif
            else
                if (present(dir )) then
                    write(unit=command,fmt='(a)') "mkdir " //trim(dir)//" 2>/dev/null"
                    status = system(command)
                    write ( file_unit, '(a)' ) 'set output "'// trim(dir)// trim(my_date_and_time())//'.'//trim(terminal)//'"'
                else
                    write ( file_unit, '(a)' ) 'set output "'//my_date_and_time()//'.'//trim(terminal)//'"'
                endif
            end if
        else
            write ( file_unit, '(a)' ) 'set terminal ' // trim(default_terminal) // ' ' &
                & //trim(my_persist) //' title  "Gnuplot"'
        end if
        !**************************************************************************
        if (present(title))then
            write ( file_unit, '(a)' ) 'set title '//"'"//title//"'"
        endif
        if (present(xlabel))then
            write ( file_unit, '(a)' ) 'set xlabel '//"'"//xlabel//"'"
        endif
        if (present(ylabel))then
            write ( file_unit, '(a)' ) 'set ylabel '//"'"//ylabel//"'"
        endif
        if (present(zlabel))then
            write ( file_unit, '(a)' ) 'set zlabel '//"'"//zlabel//"'"
        endif
        if(allocated(addition1)) write ( file_unit, '(a)' ) addition1
        if(allocated(addition2)) write ( file_unit, '(a)' ) addition2
        if(allocated(addition3)) write ( file_unit, '(a)' ) addition3
        if(allocated(addition4)) write ( file_unit, '(a)' ) addition4
        if(.not.always_add_gnuplot) then
            if(allocated(addition1)) deallocate(addition1)
            if(allocated(addition2)) deallocate(addition2)
            if(allocated(addition3)) deallocate(addition3)
            if(allocated(addition4)) deallocate(addition4)
        endif
    end subroutine initialize_command_file
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine initialize_data_file(file_unit,input)
        character(len=*),optional    :: input !style, color1, terminal, filename,dir, polar, persist, ,
        integer :: file_unit,ierror,ios,status,system
        !***********************************************************************************
        !***********************************************************************************
        if (present(input)) then
            data_file_name='data_file_'//input//'.txt'
            command_file_name='command_file_'//input//'.txt'
        else
            data_file_name='data_file.txt'
            command_file_name='command_file.txt'
        end if
            status=system(trim('del '//data_file_name))
            status=system(trim('del '//command_file_name))
        !***********************************************************************************
        ierror=0
        call get_unit(file_unit)
        if (file_unit==0) then
            ierror=1
            print *,'write_vector_date - fatal error! Could not get a free FORTRAN unit.'
            stop
        end if
        open (unit=file_unit, file=data_file_name, status='replace', iostat=ios)
        if (ios/=0) then
            ierror=2
            print *,'write_vector_data - fatal error! Could not open the terminal data file.'
            stop
        end if
    end subroutine initialize_data_file
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine pause_check(file_unit,pause)
        real(kind=4), optional        :: pause
        character(len=20)        :: my_pause
        integer ::     file_unit
        if (present(pause)) then
            if (pause<0.0) then
                write ( file_unit, '(a)' ) 'pause -1 "press RETURN to continue"'
            else
                write (    my_pause,'(e9.3)') pause
                write ( file_unit, '(a)' ) 'pause ' // trim(my_pause)
            end if
        else
            write ( file_unit, '(a)' ) 'pause -1 "press RETURN to continue"'
        end if
        !***********************************************************************************
        write ( file_unit, '(a)' ) 'q'
        close ( unit = file_unit )
    end subroutine pause_check
    subroutine available_palettes(file_unit)
        integer::file_unit
        write ( file_unit, '(a)' )'set macros'
        write ( file_unit, '(a)' )'MATLAB = "defined (0  0.0 0.0 0.5, \\'
        write ( file_unit, '(a)' )'                   1  0.0 0.0 1.0, \\'
        write ( file_unit, '(a)' )'                   2  0.0 0.5 1.0, \\'
        write ( file_unit, '(a)' )'                   3  0.0 1.0 1.0, \\'
        write ( file_unit, '(a)' )'                   4  0.5 1.0 0.5, \\'
        write ( file_unit, '(a)' )'                   5  1.0 1.0 0.0, \\'
        write ( file_unit, '(a)' )'                   6  1.0 0.5 0.0, \\'
        write ( file_unit, '(a)' )'                   7  1.0 0.0 0.0, \\'
        write ( file_unit, '(a)' )'                   8  0.5 0.0 0.0 )"'
        write ( file_unit, '(a)' )'RAINBOW="defined (0 1 0 0, 0.4 1 1 0, 0.6 0.5 1 0, 0.8 0 0 1, 1 2./3 0 1)"'
        write ( file_unit, '(a)' )'JET="define (0 0 0 0.5, 1./8 0 0 1, 3./8 0 1 1, 5./8 1 1 0, 7./8 1 0 0, 1 0.5 0 0)"'
        write ( file_unit, '(a)' )'COPPER="defined (0 0 0 0, 0.8 1 0.64 0.4, 1 1 0.8 0.5)"'
        write ( file_unit, '(a)' )'OCEAN="defined (0 0 0 0, 1./3 0 0 1./3, 2./3 0 0.5 2./3, 1 1 1 1)"'
        write ( file_unit, '(a)' )'HOT="defined (0 0 0 0, 0.4 1 0 0, 0.8 1 1 0, 1 1 1 1)"'
        write ( file_unit, '(a)' )'COOL="defined (0 0 1 1, 0 0 0 1)"'
        write ( file_unit, '(a)' )'SPRING="defined (0 0 0 .5, 0.7 1 0.64 0.9, 1 0 0.5 0)"'
        write ( file_unit, '(a)' )'SPRING1="defined (0 0 0 1, 0.8 1 0.64 0.4, 1 0 0.5 0)"'
        write ( file_unit, '(a)' )'SPRING2="defined (0 1 0 1, 0.8 1 0.64 0.4, 1 0 0.5 0)"'
        write ( file_unit, '(a)' )'SPRING3="defined (0 0 0 1, 0.8 1 0.64 0.4, 1 1 0 0)"'
        write ( file_unit, '(a)' )'SPRING4="defined (0 0 0 0, 0.8 1 0.64 0.4, 1 1 0 0)"'
        write ( file_unit, '(a)' )'SUMMER="defined (0 0 0.5 0.4, 1 1 1 0.4)" '
        write ( file_unit, '(a)' )'AUTUMN="defined (0 1 0 0, 1 1 1 0)"'
        write ( file_unit, '(a)' )'WINTER="defined (0 0 0 1, 1 0 1 0.5)"'
        write ( file_unit, '(a)' )'BONE="defined (0 0 0 0, 3./8 21.0/64 21./64 29./64, 3./4 21.0/32 25./32 25./32)"'
        write ( file_unit, '(a)' )'PINK="defined (0 0 0 0, 3./8 7./12 1./4 1./4, 3./4 5./6 5./6 1./2, 1 1 1 1)"'

    end subroutine available_palettes
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine add_gnuplot(add1,add2,add3,add4,always)
        implicit none
        character(len=*),optional :: add1,add2,add3,add4,always

        if(present(add2)) addition2=add2
        if(present(add3)) addition3=add3
        if(present(add4)) addition4=add4
        if(present(add1)) then
            if(.not.allocated(addition1)) then
                addition1=add1
            else if(.not.allocated(addition2)) then
                addition2=add1
            else if(.not.allocated(addition3)) then
                addition3=add1
            else if(.not.allocated(addition4)) then
                addition4=add1
            endif
        endif
        if(.not.(present(add1).or.present(add2).or.present(add3).or.present(add4)))then
            if(present(always)) then
                if((always=='yes').or.(always=='YES').or.(always=='y').or.(always=='Y')) then
                    always_add_gnuplot=.true.
                else
                    call reset_add_gnuplot()
                endif
            else
                call reset_add_gnuplot()
            endif
        elseif(present(always)) then
            if((always=='yes').or.(always=='YES').or.(always=='y').or.(always=='Y')) then
                always_add_gnuplot=.true.
            else
                always_add_gnuplot=.false.
            endif
        else
            always_add_gnuplot=.false.
        endif
    end subroutine add_gnuplot
    subroutine reset_add_gnuplot()
        always_add_gnuplot=.false.
        if(allocated(addition1)) deallocate(addition1)
        if(allocated(addition2)) deallocate(addition2)
        if(allocated(addition3)) deallocate(addition3)
        if(allocated(addition4)) deallocate(addition4)
    end subroutine reset_add_gnuplot
end module gnufor3





!program test
!    use gnufor3
!    implicit none
!    !***********************************************************************************
!    integer, parameter    :: N1=50, N2=100, N3=200, N4=500, Nx=100, Ny=100, Nm=800
!    real(kind=8)        :: x1(N1), x2(N2), x3(N3), x4(N4)
!    real(kind=8)        :: f1(N1), f2(N2), f3(N3), f4(N4)
!    real(kind=8)        :: u1(1000), u2(1000), u3(1000), t(1000)
!    real(kind=8)        :: xx(Nx), yy(Ny), zz(Nx,Ny), xyz(3,Nx,Ny), u(Nx), v(Nx)
!    real(kind=4)        :: z_fractal(Nm,Nm), x_fractal(Nm), y_fractal(Nm), &
!        & xstart,ystart,xend,yend,zmax,escape,color_scale,z_power
!    integer            :: rgb(3,Nm,Nm)
!    complex(kind=8)        :: z, a
!    integer            :: i,j,k, N0
!    character(len=100) :: temp
!    character           :: yesno
!    !***********************************************************************************
!    !***********************************************************************************
!    call set_echo('yes')
!    !***********************************************************************************
!    call generate_data_for_2D_plots()
!    !***********************************************************************************
!    call generate_data_for_3D_plot_of_a_curve()
!    !***********************************************************************************
!    call generate_data1_for_surface_plots()
!    call generate_data2_for_surface_plots()
!    !***********************************************************************************
!    call create_date_for_fractal_plot()
!    !***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Simple 2D graph example'
!    print *,'call plot(x1,f1)'
!    call plot(x1,f1 ,title="نمودار دو بعدی ساده",xlabel="محور زمان",ylabel="محور y")
!    !***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Simple 2D graph\n gnuplot is opening an you can adding other gnuplot command'
!    print *,'call plot(x1,f1,persist=''yes'')'
!    print *,'!!!!!!!!!! for exit gnuplot please press q !!!!!!!!!!'
!    call plot(x1,f1,persist='yes')
!    !***********************************************************************************
!
!
!    print *,'********************************************************************'
!    print *,'style parameter\n  plotting with points only Examples\n(y or n)?'
!    read*,yesno
!    if(yesno(1:1)=='y'.or.yesno(1:1)=='Y') call plotting_with_points_only()
!    !***********************************************************************************
!    !***********************************************************************************
!    print *,'style parameter\n  plotting with lines Examples\n(y or n)?'
!    read*,yesno
!    if(yesno(1:1)=='y'.or.yesno(1:1)=='Y') call plotting_with_lines()
!    ! ***********************************************************************************
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 1: specifying color'
!    print *,'call plot(x1,f1,''21-'',color1=''red'')'
!    call plot(x1,f1,'21-' ,color1='red')
!    ! ***********************************************************************************
!
!
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 2: specifying the linewidth'
!    print *,'call plot(x1,f1,'' 0-'',linewidth=3.5)'
!    call plot(x1,f1,' 0-' ,linewidth=3.5)
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 3: specifying the specific command/data file name'
!    print *,'call plot(x1,f1,'' 8-'',input=''my_filename'')'
!    call plot(x1,f1,' 8-' ,input='my_filename')
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 12: 2D graphs in polar coordinates'
!    print *,'call plot(x1,f1,'' 8-'',polar=''yes'')'
!    call plot(x1,f1,' 8-' ,polar='yes')
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 4: several graphs in the same coordinate system'
!    print *,'call plot(x1,f1,x2,f2,x3,f3)'
!    call plot(x1,f1,x2,f2,x3,f3 )
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 5: several graphs in the same coordinate system'
!    print *,'call plot(x1,f1,x2,f2,x3,f3,x4,f4,&
!        &'' 5.22- 0- 0-'',color2=''dark-yellow'',color1=''#40e0d0'')'
!    call plot(x1,f1,x2,f2,x3,f3,x4,f4,' 5.22- 0- 0-',color2='dark-yellow',color1='#40e0d0' )
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 6: several graphs in the same polar coordinate system'
!    print *,'call plot(x1,f1,x2,f2,x3,f3,x4,f4,polar=''yes'',linewidth=2.0)'
!    call plot(x1,f1,x2,f2,x3,f3,x4,f4 ,polar='yes',linewidth=2.0)
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 7: histogram with 10 bins'
!    print *,'call hist(f1,10,color=''#779944'')'
!    call  hist(f1,10,color='#779944' )
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 8: 3D plot of a curve'
!    print *,'call plot3D(u1,u2,u3,linewidth=2.0)'
!    call  plot3D(u1,u2,u3 ,linewidth=2.0)
!    ! ***********************************************************************************
!
!    !***********************************************************************************
!    print *,'surface plots\n  plotting surface plots Examples\n(y or n)?'
!    read*,yesno
!    if(yesno(1:1)=='y'.or.yesno(1:1)=='Y') call surface_plots()
!
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 1: image plot'
!    print *,'call image(z_fractal,palette=''gray'')'
!    call  image(z_fractal,palette='gray' )
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 2: image plot with specified x and y axes'
!    print *,'call image(x_fractal,y_fractal,z_fractal)'
!    call  image(x_fractal,y_fractal,z_fractal )
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 3: RGB image plot'
!    print *,'call  image(rgb)'
!    call  image(rgb )
!    ! ***********************************************************************************
!    print *,'********************************************************************'
!    print *,'Example 4: RGB image plot with specified x and y axes'
!    print *,'call  image(x_fractal,y_fractal,rgb)'
!    call  image(x_fractal,y_fractal,rgb )
!    !***********************************************************************************
!    !***********************************************************************************
!    !***********************************************************************************
!contains
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    subroutine generate_data_for_2D_plots()
!        do  i=1,N1
!            x1(i)=5.0*i/N1
!        end do
!        do  i=1,N2
!            x2(i)=6.0*i/N2
!        end do
!        do  i=1,N3
!            x3(i)=7.0*i/N3
!        end do
!        do  i=1,N4
!            x4(i)=8.0*i/N4
!        end do
!        f1=sin(2*x1)
!        f2=4*x2/(1+x2**2)
!        f3=4-x3
!        f4=exp(x4/3)-5
!    end subroutine generate_data_for_2D_plots
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    subroutine generate_data_for_3D_plot_of_a_curve()
!        do i=1,1000
!            t(i)=80.0*i/1000
!        end do
!        u1=4*cos(t)-cos(5*t)
!        u2=4*sin(t)-sin(5*t)
!        u3=10*sin(t/7)
!    end subroutine generate_data_for_3D_plot_of_a_curve
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    subroutine generate_data1_for_surface_plots()
!        do i=1,Nx
!            xx(i)=6.0*(dble(i)/Nx-0.5)
!        end do
!        do i=1,Ny
!            yy(i)=6.0*(dble(i)/Ny-0.5)
!        end do
!        do i=1,Nx
!            do j=1,Ny
!                zz(i,j)=1-exp(-(xx(i)-1)**2-yy(j)**2)
!            end do
!        end do
!    end subroutine generate_data1_for_surface_plots
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    subroutine generate_data2_for_surface_plots()
!        do i=1,Nx
!            u(i)=1.5*3.15*(dble(i)/Nx)
!        end do
!        do i=1,Ny
!            v(i)=1.6*3.15*(dble(i)/Ny)+0.7
!        end do
!        do i=1,Nx
!            do j=1,Ny
!                xyz(1,i,j)=(3+cos(v(j)))*cos(u(i))
!                xyz(2,i,j)=(3+cos(v(j)))*sin(u(i))
!                xyz(3,i,j)=sin(v(j))
!                xyz(1,i,j)=xyz(1,i,j)-0.5*xyz(2,i,j)-0.5*xyz(3,i,j)
!                xyz(3,i,j)=xyz(2,i,j)-5*xyz(3,i,j)
!            end do
!        end do
!    end subroutine generate_data2_for_surface_plots
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    subroutine create_date_for_fractal_plot()
!        implicit none
!        xstart=-2.0
!        xend=1.0
!        ystart=-1.5
!        yend=1.5
!        escape=0.85
!        z_power=2.0
!        color_scale=0.2
!        zmax=exp(log(10.0)*escape)
!        N0=150
!        z_fractal=1.0
!        do i=1,Nm
!            x_fractal(i)=(xstart+(xend-xstart)*(real(i-1)/(Nm-1)))
!            do j=1,Nm
!                y_fractal(j)=(ystart+(yend-ystart)*(real(j-1)/(Nm-1)))
!                a=x_fractal(i)+y_fractal(j)*(0.0,1.0)
!                z=a
!                k=1
!                do while ((abs(z)<zmax).and.(k<N0))
!                    k=k+1
!                    z=z*z+a
!                end do
!                if (k<N0) then
!                    z_fractal(i,j)=k+(log(abs(z))/(escape*log(10.0))-1.0)/(1.0-z_power)
!                    z_fractal(i,j)=0.5*(1.0+sin(z_fractal(i,j)*color_scale))
!                end if
!                rgb(1,i,j)=floor(z_fractal(i,j)*255)
!                rgb(2,i,j)=floor(255*(atan(aimag(z)/real(z))/1.58+1)/2.0)
!                rgb(3,i,j)=floor(255*(1+cos(abs(z)/5.0))/2.0)
!            end do
!        end do
!    end subroutine create_date_for_fractal_plot
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    subroutine plotting_with_points_only()
!        print *,'********************************************************************'
!        print *,'Example 1: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 1.'')'
!        call plot(x1,f1,style=' 1.',title="command is: call plot(x1,f1,'' 1.'') ")
!        print *,'********************************************************************'
!        print *,'Example 2: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 2.'')'
!        call plot(x1,f1,style=' 2.',title="command is: call plot(x1,f1,'' 2.'') ")
!        print *,'********************************************************************'
!        print *,'Example 3: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 3.'')'
!        call plot(x1,f1,style=' 3.',title="command is: call plot(x1,f1,'' 3.'') ")
!        print *,'********************************************************************'
!        print *,'Example 4: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 4.'')'
!        call plot(x1,f1,style=' 4.',title="command is: call plot(x1,f1,'' 4.'') ")
!        print *,'********************************************************************'
!        print *,'Example 5: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 5.'')'
!        call plot(x1,f1,style=' 5.',title="command is: call plot(x1,f1,'' 5.'') ")
!        print *,'********************************************************************'
!        print *,'Example 6: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 6.'')'
!        call plot(x1,f1,style=' 6.',title="command is: call plot(x1,f1,'' 6.'') ")
!        print *,'********************************************************************'
!        print *,'Example 7: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 7.'')'
!        call plot(x1,f1,style=' 7.',title="command is: call plot(x1,f1,'' 7.'') ")
!        print *,'********************************************************************'
!        print *,'Example 8: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 8.'')'
!        call plot(x1,f1,style=' 8.',title="command is: call plot(x1,f1,'' 8.'') ")
!        print *,'********************************************************************'
!        print *,'Example 9: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 9.'')'
!        call plot(x1,f1,style=' 9.',title="command is: call plot(x1,f1,'' 9.'') ")
!    end subroutine plotting_with_points_only
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    subroutine plotting_with_lines()
!        print *,'********************************************************************'
!        print *,'Example 0: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 0-'')'
!        call plot(x1,f1,style=' 0-',title="command is: call plot(x1,f1,'' 0-'') ")
!        print *,'Example 1: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 1-'')'
!        call plot(x1,f1,style=' 1-',title="command is: call plot(x1,f1,'' 1-'') ")
!        print *,'Example 2: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' -'')'
!        call plot(x1,f1,style=' 2-',title="command is: call plot(x1,f1,'' 2-'') ")
!        print *,'Example 3: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' -'')'
!        call plot(x1,f1,style=' 3-',title="command is: call plot(x1,f1,'' 3-'') ")
!        print *,'Example 4: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' -'')'
!        call plot(x1,f1,style=' 4-',title="command is: call plot(x1,f1,'' 4-'') ")
!        print *,'Example 5: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' -'')'
!        call plot(x1,f1,style=' 5-',title="command is: call plot(x1,f1,'' 5-'') ")
!        print *,'Example 6: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' -'')'
!        call plot(x1,f1,style=' 6-',title="command is: call plot(x1,f1,'' 6-'') ")
!        print *,'Example 7: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' -'')'
!        call plot(x1,f1,style=' 7-',title="command is: call plot(x1,f1,'' 7-'') ")
!        print *,'Example 8: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' -'')'
!        call plot(x1,f1,style=' 8-',title="command is: call plot(x1,f1,'' 8-'') ")
!        print *,'Example 9: style parameter - plotting with points only'
!        print *,'call plot(x1,f1,'' 9-'')'
!        call plot(x1,f1,style=' 9-',title="command is: call plot(x1,f1,'' 9-'') ")
!    end subroutine plotting_with_lines
!    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!    subroutine surface_plots()
!        print *,'********************************************************************'
!        print *,'Example 1: surface plot'
!        print *,'call surf(xx,yy,zz)'
!        call  surf(xx,yy,zz)
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 2: surface plot without specying x and y grid'
!        print *,'call surf(zz)'
!        call  surf(zz)
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 3: parametric surface plot'
!        print *,'call surf(xyz)'
!        call  surf(xyz)
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 4: parametric surface plot'
!        print *,'call surf(xyz,pm3d=''pm3d'')'
!        call  surf(xyz,pm3d='pm3d' )
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 5: parametric surface plot, different palette'
!        print *,'call surf(xyz,pm3d=''pm3d'',palette=''RGB'')'
!        call  surf(xyz,pm3d='pm3d',palette='gray',title="command=  call  surf(xyz,pm3d=''pm3d'',palette=''gray'')")
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 6: parametric surface plot, different palette. MATLAB is a user defined palette in module gnufor3'
!        print *,'call surf(xyz,pm3d=''pm3d'',palette=''@MATLAB'')'
!        call  surf(xyz,pm3d='pm3d',palette='@MATLAB',title="command= call  surf(xyz,pm3d=''pm3d'',palette=''\\@MATLAB'')" )
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 7: parametric surface plot, different palette'
!        print *,'call surf(xyz,pm3d=''pm3d'',palette=''gray negative'')'
!        call  surf(xyz,pm3d='pm3d',palette='gray negative' )
!        ! ***********************************************************************************
!        print *,'*******************************************************************'
!        print *,'Example 8: parametric surface plot, different palette'
!        print *,'call surf(xyz,pm3d=''pm3d'',palette=''rgbformulae 23,28,3'')'
!        call  surf(xyz,pm3d='pm3d',palette='rgbformulae 23,28,3' )
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 9: surface plot'
!        print *,'call surf(xx,yy,zz,pm3d=''pm3d at b'',palette=''HSV'')'
!        call  surf(xx,yy,zz,pm3d='pm3d at b',palette='HSV' )
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 10: surface plot'
!        print *,'call surf(xx,yy,zz,pm3d=''pm3d implicit map'',palette=''rgbformulae 31,-11,32'')'
!        call  surf(xx,yy,zz,pm3d='pm3d implicit map',palette='rgbformulae 31,-11,32' )
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 11: surface plot with contour on the surface'
!        print *,'call surf(xx,yy,zz,contour=''surface'',palette=''YIQ'')'
!        call  surf(xx,yy,zz,contour='surface',palette='YIQ' )
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 12: surface plot with contour on the XY plane. RAINBOW is a user defined palette in module gnufor3'
!        print *,'call surf(xx,yy,zz,contour=''xy'')'
!        call  surf(xx,yy,zz,contour='xy',palette='@RAINBOW',title="command=  call  surf(xx,yy,zz,contour=''both'',palette=''\\@RAINBOW'' )" )
!        ! ***********************************************************************************
!        print *,'********************************************************************'
!        print *,'Example 13: surface plot with contour on the surface and the XY plane.  OCEAN is a user defined palette in module gnufor3'
!        print *,'call surf(xx,yy,zz,contour=''both'')'
!        call  surf(xx,yy,zz,contour='both',palette='@OCEAN',title="command=  call  surf(xx,yy,zz,contour=''both'',palette=''\\@OCEAN'' )")
!        print *,'********************************************************************'
!        print *,'Example 14: surface plot with contour on the surface and the XY plane.  COPPER is a user defined palette in module gnufor3'
!        print *,'call surf(xx,yy,zz,contour=''both'')'
!        call  surf(xx,yy,zz,contour='both',palette='@COPPER',title="command=  call  surf(xx,yy,zz,contour=''both'',palette=''\\@COPPER'' )")
!    end subroutine surface_plots
!end program test

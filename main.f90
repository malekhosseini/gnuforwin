program test
    use gnufor3
    implicit none
    !***********************************************************************************
    integer, parameter    :: N1=50, N2=100, N3=200, N4=500, Nx=100, Ny=100, Nm=800
    real(kind=8)        :: x1(N1), x2(N2), x3(N3), x4(N4)
    real(kind=8)        :: f1(N1), f2(N2), f3(N3), f4(N4)
    real(kind=8)        :: u1(1000), u2(1000), u3(1000), t(1000)
    real(kind=8)        :: xx(Nx), yy(Ny), zz(Nx,Ny), xyz(3,Nx,Ny), u(Nx), v(Nx)
    real(kind=4)        :: z_fractal(Nm,Nm), x_fractal(Nm), y_fractal(Nm), &
        & xstart,ystart,xend,yend,zmax,escape,color_scale,z_power
    integer            :: rgb(3,Nm,Nm)
    complex(kind=8)        :: z, a
    integer            :: i,j,k, N0
    character(len=100) :: temp
    character           :: yesno
    !***********************************************************************************
    !***********************************************************************************
    call set_echo('yes')
    !***********************************************************************************
    call generate_data_for_2D_plots()
    !***********************************************************************************
    call generate_data_for_3D_plot_of_a_curve()
    !***********************************************************************************
    call generate_data1_for_surface_plots()
    call generate_data2_for_surface_plots()
    !***********************************************************************************
    call create_date_for_fractal_plot()
    !***********************************************************************************
    print *,'********************************************************************'
    print *,'Simple 2D graph example'
    print *,'call plot(x1,f1)'
    call plot(x1,f1 ,title="نمودار دو بعدی ساده",xlabel="محور زمان",ylabel="محور y")
    !***********************************************************************************
    print *,'********************************************************************'
    print *,'Simple 2D graph\n gnuplot is opening an you can adding other gnuplot command'
    print *,'call plot(x1,f1,persist=''yes'')'
    print *,'!!!!!!!!!! for exit gnuplot please press q !!!!!!!!!!'
    call plot(x1,f1,persist='yes')
    !***********************************************************************************


    print *,'********************************************************************'
    print *,'style parameter\n  plotting with points only Examples\n(y or n)?'
    read*,yesno
    if(yesno(1:1)=='y'.or.yesno(1:1)=='Y') call plotting_with_points_only()
    !***********************************************************************************
    !***********************************************************************************
    print *,'style parameter\n  plotting with lines Examples\n(y or n)?'
    read*,yesno
    if(yesno(1:1)=='y'.or.yesno(1:1)=='Y') call plotting_with_lines()
    ! ***********************************************************************************
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 1: specifying color'
    print *,'call plot(x1,f1,''21-'',color1=''red'')'
    call plot(x1,f1,'21-' ,color1='red')
    ! ***********************************************************************************


    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 2: specifying the linewidth'
    print *,'call plot(x1,f1,'' 0-'',linewidth=3.5)'
    call plot(x1,f1,' 0-' ,linewidth=3.5)
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 3: specifying the specific command/data file name'
    print *,'call plot(x1,f1,'' 8-'',input=''my_filename'')'
    call plot(x1,f1,' 8-' ,input='my_filename')
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 12: 2D graphs in polar coordinates'
    print *,'call plot(x1,f1,'' 8-'',polar=''yes'')'
    call plot(x1,f1,' 8-' ,polar='yes')
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 4: several graphs in the same coordinate system'
    print *,'call plot(x1,f1,x2,f2,x3,f3)'
    call plot(x1,f1,x2,f2,x3,f3 )
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 5: several graphs in the same coordinate system'
    print *,'call plot(x1,f1,x2,f2,x3,f3,x4,f4,&
        &'' 5.22- 0- 0-'',color2=''dark-yellow'',color1=''#40e0d0'')'
    call plot(x1,f1,x2,f2,x3,f3,x4,f4,' 5.22- 0- 0-',color2='dark-yellow',color1='#40e0d0' )
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 6: several graphs in the same polar coordinate system'
    print *,'call plot(x1,f1,x2,f2,x3,f3,x4,f4,polar=''yes'',linewidth=2.0)'
    call plot(x1,f1,x2,f2,x3,f3,x4,f4 ,polar='yes',linewidth=2.0)
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 7: histogram with 10 bins'
    print *,'call hist(f1,10,color=''#779944'')'
    call  hist(f1,10,color='#779944' )
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 8: 3D plot of a curve'
    print *,'call plot3D(u1,u2,u3,linewidth=2.0)'
    call  plot3D(u1,u2,u3 ,linewidth=2.0)
    ! ***********************************************************************************

    !***********************************************************************************
    print *,'surface plots\n  plotting surface plots Examples\n(y or n)?'
    read*,yesno
    if(yesno(1:1)=='y'.or.yesno(1:1)=='Y') call surface_plots()

    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 1: image plot'
    print *,'call image(z_fractal,palette=''gray'')'
    call  image(z_fractal,palette='gray' )
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 2: image plot with specified x and y axes'
    print *,'call image(x_fractal,y_fractal,z_fractal)'
    call  image(x_fractal,y_fractal,z_fractal )
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 3: RGB image plot'
    print *,'call  image(rgb)'
    call  image(rgb )
    ! ***********************************************************************************
    print *,'********************************************************************'
    print *,'Example 4: RGB image plot with specified x and y axes'
    print *,'call  image(x_fractal,y_fractal,rgb)'
    call  image(x_fractal,y_fractal,rgb )
    !***********************************************************************************
    !***********************************************************************************
    !***********************************************************************************
contains
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine generate_data_for_2D_plots()
        do  i=1,N1
            x1(i)=5.0*i/N1
        end do
        do  i=1,N2
            x2(i)=6.0*i/N2
        end do
        do  i=1,N3
            x3(i)=7.0*i/N3
        end do
        do  i=1,N4
            x4(i)=8.0*i/N4
        end do
        f1=sin(2*x1)
        f2=4*x2/(1+x2**2)
        f3=4-x3
        f4=exp(x4/3)-5
    end subroutine generate_data_for_2D_plots
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine generate_data_for_3D_plot_of_a_curve()
        do i=1,1000
            t(i)=80.0*i/1000
        end do
        u1=4*cos(t)-cos(5*t)
        u2=4*sin(t)-sin(5*t)
        u3=10*sin(t/7)
    end subroutine generate_data_for_3D_plot_of_a_curve
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine generate_data1_for_surface_plots()
        do i=1,Nx
            xx(i)=6.0*(dble(i)/Nx-0.5)
        end do
        do i=1,Ny
            yy(i)=6.0*(dble(i)/Ny-0.5)
        end do
        do i=1,Nx
            do j=1,Ny
                zz(i,j)=1-exp(-(xx(i)-1)**2-yy(j)**2)
            end do
        end do
    end subroutine generate_data1_for_surface_plots
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine generate_data2_for_surface_plots()
        do i=1,Nx
            u(i)=1.5*3.15*(dble(i)/Nx)
        end do
        do i=1,Ny
            v(i)=1.6*3.15*(dble(i)/Ny)+0.7
        end do
        do i=1,Nx
            do j=1,Ny
                xyz(1,i,j)=(3+cos(v(j)))*cos(u(i))
                xyz(2,i,j)=(3+cos(v(j)))*sin(u(i))
                xyz(3,i,j)=sin(v(j))
                xyz(1,i,j)=xyz(1,i,j)-0.5*xyz(2,i,j)-0.5*xyz(3,i,j)
                xyz(3,i,j)=xyz(2,i,j)-5*xyz(3,i,j)
            end do
        end do
    end subroutine generate_data2_for_surface_plots
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine create_date_for_fractal_plot()
        implicit none
        xstart=-2.0
        xend=1.0
        ystart=-1.5
        yend=1.5
        escape=0.85
        z_power=2.0
        color_scale=0.2
        zmax=exp(log(10.0)*escape)
        N0=150
        z_fractal=1.0
        do i=1,Nm
            x_fractal(i)=(xstart+(xend-xstart)*(real(i-1)/(Nm-1)))
            do j=1,Nm
                y_fractal(j)=(ystart+(yend-ystart)*(real(j-1)/(Nm-1)))
                a=x_fractal(i)+y_fractal(j)*(0.0,1.0)
                z=a
                k=1
                do while ((abs(z)<zmax).and.(k<N0))
                    k=k+1
                    z=z*z+a
                end do
                if (k<N0) then
                    z_fractal(i,j)=k+(log(abs(z))/(escape*log(10.0))-1.0)/(1.0-z_power)
                    z_fractal(i,j)=0.5*(1.0+sin(z_fractal(i,j)*color_scale))
                end if
                rgb(1,i,j)=floor(z_fractal(i,j)*255)
                rgb(2,i,j)=floor(255*(atan(aimag(z)/real(z))/1.58+1)/2.0)
                rgb(3,i,j)=floor(255*(1+cos(abs(z)/5.0))/2.0)
            end do
        end do
    end subroutine create_date_for_fractal_plot
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine plotting_with_points_only()
        print *,'********************************************************************'
        print *,'Example 1: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 1.'')'
        call plot(x1,f1,style=' 1.',title="command is: call plot(x1,f1,'' 1.'') ")
        print *,'********************************************************************'
        print *,'Example 2: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 2.'')'
        call plot(x1,f1,style=' 2.',title="command is: call plot(x1,f1,'' 2.'') ")
        print *,'********************************************************************'
        print *,'Example 3: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 3.'')'
        call plot(x1,f1,style=' 3.',title="command is: call plot(x1,f1,'' 3.'') ")
        print *,'********************************************************************'
        print *,'Example 4: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 4.'')'
        call plot(x1,f1,style=' 4.',title="command is: call plot(x1,f1,'' 4.'') ")
        print *,'********************************************************************'
        print *,'Example 5: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 5.'')'
        call plot(x1,f1,style=' 5.',title="command is: call plot(x1,f1,'' 5.'') ")
        print *,'********************************************************************'
        print *,'Example 6: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 6.'')'
        call plot(x1,f1,style=' 6.',title="command is: call plot(x1,f1,'' 6.'') ")
        print *,'********************************************************************'
        print *,'Example 7: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 7.'')'
        call plot(x1,f1,style=' 7.',title="command is: call plot(x1,f1,'' 7.'') ")
        print *,'********************************************************************'
        print *,'Example 8: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 8.'')'
        call plot(x1,f1,style=' 8.',title="command is: call plot(x1,f1,'' 8.'') ")
        print *,'********************************************************************'
        print *,'Example 9: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 9.'')'
        call plot(x1,f1,style=' 9.',title="command is: call plot(x1,f1,'' 9.'') ")
    end subroutine plotting_with_points_only
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine plotting_with_lines()
        print *,'********************************************************************'
        print *,'Example 0: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 0-'')'
        call plot(x1,f1,style=' 0-',title="command is: call plot(x1,f1,'' 0-'') ")
        print *,'Example 1: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 1-'')'
        call plot(x1,f1,style=' 1-',title="command is: call plot(x1,f1,'' 1-'') ")
        print *,'Example 2: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' -'')'
        call plot(x1,f1,style=' 2-',title="command is: call plot(x1,f1,'' 2-'') ")
        print *,'Example 3: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' -'')'
        call plot(x1,f1,style=' 3-',title="command is: call plot(x1,f1,'' 3-'') ")
        print *,'Example 4: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' -'')'
        call plot(x1,f1,style=' 4-',title="command is: call plot(x1,f1,'' 4-'') ")
        print *,'Example 5: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' -'')'
        call plot(x1,f1,style=' 5-',title="command is: call plot(x1,f1,'' 5-'') ")
        print *,'Example 6: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' -'')'
        call plot(x1,f1,style=' 6-',title="command is: call plot(x1,f1,'' 6-'') ")
        print *,'Example 7: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' -'')'
        call plot(x1,f1,style=' 7-',title="command is: call plot(x1,f1,'' 7-'') ")
        print *,'Example 8: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' -'')'
        call plot(x1,f1,style=' 8-',title="command is: call plot(x1,f1,'' 8-'') ")
        print *,'Example 9: style parameter - plotting with points only'
        print *,'call plot(x1,f1,'' 9-'')'
        call plot(x1,f1,style=' 9-',title="command is: call plot(x1,f1,'' 9-'') ")
    end subroutine plotting_with_lines
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    subroutine surface_plots()
        print *,'********************************************************************'
        print *,'Example 1: surface plot'
        print *,'call surf(xx,yy,zz)'
        call  surf(xx,yy,zz)
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 2: surface plot without specying x and y grid'
        print *,'call surf(zz)'
        call  surf(zz)
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 3: parametric surface plot'
        print *,'call surf(xyz)'
        call  surf(xyz)
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 4: parametric surface plot'
        print *,'call surf(xyz,pm3d=''pm3d'')'
        call  surf(xyz,pm3d='pm3d' )
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 5: parametric surface plot, different palette'
        print *,'call surf(xyz,pm3d=''pm3d'',palette=''RGB'')'
        call  surf(xyz,pm3d='pm3d',palette='gray',title="command=  call  surf(xyz,pm3d=''pm3d'',palette=''gray'')")
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 6: parametric surface plot, different palette. MATLAB is a user defined palette in module gnufor3'
        print *,'call surf(xyz,pm3d=''pm3d'',palette=''@MATLAB'')'
        call  surf(xyz,pm3d='pm3d',palette='@MATLAB',title="command= call  surf(xyz,pm3d=''pm3d'',palette=''\\@MATLAB'')" )
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 7: parametric surface plot, different palette'
        print *,'call surf(xyz,pm3d=''pm3d'',palette=''gray negative'')'
        call  surf(xyz,pm3d='pm3d',palette='gray negative' )
        ! ***********************************************************************************
        print *,'*******************************************************************'
        print *,'Example 8: parametric surface plot, different palette'
        print *,'call surf(xyz,pm3d=''pm3d'',palette=''rgbformulae 23,28,3'')'
        call  surf(xyz,pm3d='pm3d',palette='rgbformulae 23,28,3' )
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 9: surface plot'
        print *,'call surf(xx,yy,zz,pm3d=''pm3d at b'',palette=''HSV'')'
        call  surf(xx,yy,zz,pm3d='pm3d at b',palette='HSV' )
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 10: surface plot'
        print *,'call surf(xx,yy,zz,pm3d=''pm3d implicit map'',palette=''rgbformulae 31,-11,32'')'
        call  surf(xx,yy,zz,pm3d='pm3d implicit map',palette='rgbformulae 31,-11,32' )
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 11: surface plot with contour on the surface'
        print *,'call surf(xx,yy,zz,contour=''surface'',palette=''YIQ'')'
        call  surf(xx,yy,zz,contour='surface',palette='YIQ' )
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 12: surface plot with contour on the XY plane. RAINBOW is a user defined palette in module gnufor3'
        print *,'call surf(xx,yy,zz,contour=''xy'')'
        call  surf(xx,yy,zz,contour='xy',palette='@RAINBOW',title="command=  call  &
                    surf(xx,yy,zz,contour=''both'',palette=''\\@RAINBOW'' )" )
        ! ***********************************************************************************
        print *,'********************************************************************'
        print *,'Example 13: surface plot with contour on the surface and the XY plane.  OCEAN is a &
                    user defined palette in module gnufor3'
        print *,'call surf(xx,yy,zz,contour=''both'')'
        call  surf(xx,yy,zz,contour='both',palette='@OCEAN',title="command=  call  surf(xx,yy,zz, &
                    contour=''both'',palette=''\\@OCEAN'' )")
        print *,'********************************************************************'
        print *,'Example 14: surface plot with contour on the surface and the XY plane.  COPPER is a user &
                    defined palette in module gnufor3'
        print *,'call surf(xx,yy,zz,contour=''both'')'
        call  surf(xx,yy,zz,contour='both',palette='@COPPER',title="command=  call  surf(xx,yy,zz, &
                    contour=''both'',palette=''\\@COPPER'' )")
    end subroutine surface_plots
end program test
